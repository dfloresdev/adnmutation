const { MongoClient } = require("mongodb");
import "babel-polyfill";
const mongoDB = require("../../src/store/mongodb");
var ObjectId = require("mongodb").ObjectID;

const mockArray = ["ATGCGA", "CAGTGC", "TTATTT", "AGACGG", "GCGTCA", "TCACTG"];

describe("Test Database", () => {
  let connection;
  let db;

  beforeAll(async () => {
    connection = await MongoClient.connect(global.__MONGO_URI__, {
      useNewUrlParser: true,
    });
    db = await connection.db(global.__MONGO_DB_NAME__);
  });

  afterAll(async () => {
    await connection.close();
    await db.close();
  });

  test("Test insert and get stats of mongodb", async () => {
    const dnas = db.collection("dnas");

    const mockDnad = {
      _id: new ObjectId("5f80ce5d1c6cea04a4c21b8c"),
      dna: mockArray,
      mutation: true,
    };

    await mongoDB.insert("dnaRecords", mockDnad);
    const stats = await mongoDB.getStats();

    const mockDna = { _id: "some-dna-id", dna: mockArray, mutation: true };
    await dnas.insertOne(mockDna);

    const dna = await dnas.findOne({ _id: "some-dna-id" });
    expect(dna).toEqual(mockDna);
  });
});
