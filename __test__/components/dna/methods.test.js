const methods = require("../../../src/api/components/dna/methods");

const mockArray = ["ATGCGA", "CAGTGC", "TTATTT", "AGACGG", "GCGTCA", "TCACTG"];
const mockArrayColumns = [
  "ACTAGT",
  "TATGCC",
  "GGAAGA",
  "CTTCTC",
  "GGTGCT",
  "ACTGAG",
];
const mockArrayColumnsTopLeft = ["CGTA", "GTAGG", "AGTACT", "CTCGC", "TGTA"];
const getColumnsTopRight = ["GTTG", "TGTGA", "AAACCG", "CTATT", "TGGC"];
const mockArrayMutation = [
  "ATGCGA",
  "CAGTGC",
  "TTATGT",
  "AGAAGG",
  "CCCCTA",
  "TCACTG",
];

describe("Testing all methods to analize a chain of DNA", () => {
  test("Testing method checkChain", () => {
    expect(methods.checkChain("ATGGTT", 0)).toBeTruthy();
    expect(methods.checkChain("ATGGTTTT", 0)).toBeFalsy();
    expect(methods.checkChain("ATGGG", 1)).toBeFalsy();
    expect(methods.checkChain("ATGG", 2)).toBeTruthy();
    expect(methods.checkChain("ATGGGG", 3)).toBeFalsy();
    expect(methods.checkChain("ATG", 3)).toBeFalsy();
  });

  test("Testing method chainRows", () => {
    expect(methods.chainRows(mockArray, 1)).toBeUndefined();
    expect(methods.chainRows(mockArrayMutation, 1)).toEqual("CCCCTA");
  });

  test("Testing method getColumns", () => {
    expect(methods.getColumns(mockArray)).toEqual(mockArrayColumns);
  });

  test("Testing method getColumnsTopLeft", () => {
    expect(methods.getColumnsTopLeft(mockArray)).toEqual(
      mockArrayColumnsTopLeft,
    );
  });

  test("Testing method getColumnsTopLeft", () => {
    expect(methods.getColumnsTopRight(mockArray)).toEqual(getColumnsTopRight);
  });

  test("Testing method getSideways", () => {
    expect(methods.getSideways(6)).toBe(5);
    expect(methods.getSideways(7)).toBe(7);
    expect(methods.getSideways(8)).toBe(9);
    expect(methods.getSideways(9)).toBe(11);
  });
});
