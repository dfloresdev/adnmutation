const request = require("supertest");
const app = require("../../src/api/app");

const mockArray = {
  dna: ["ATGCGA", "CAGTGC", "TTATTT", "AGACGG", "GCGTCA", "TCACTG"],
};

describe("probando api", () => {
  test("el test get", () => {
    return request(app)
      .get("/")
      .then((response) => {
        console.log(response.body);
        expect(response.statusCode).toBe(200);
      });
  });
});

describe("probando api de dna", () => {
  test("el test post", () => {
    return request(app)
      .post("/api/dna/mutation")
      .send(mockArray)
      .then((response) => {
        console.log(response.body);
        expect(response.statusCode).toBe(500);
      });
  });
});

describe("probando api de dna", () => {
  test("el test post", () => {
    return request(app)
      .post("/api/dna/stats")
      .send(mockArray)
      .then((response) => {
        console.log(response.body);
        expect(response.statusCode).toBe(404);
      });
  });
});
