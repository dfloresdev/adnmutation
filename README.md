# DNA Mutation 🧬🛠

In this repo have a interesting problem, it's simple, detect mutation in DNA chains

This project have two endpoints:

1. GET /stats
2. POST /mutation

You can see the endpoints and documentation **[in this link](https://mutation-a2b2dj5kva-uc.a.run.app)**

### Problem

The user insert a _array_ of strings (only **A**, **T**, **C** or **G**), always **N\*N**

The DNA has mutation when has four or more repeated letters in form oblique, horizontal or vertical

Example:

- No mutation
  ![no mutation](https://i.imgur.com/a7cwH55.png)

- Mutation
  ![no mutation](https://i.imgur.com/qnzojat.png)

### About the project

- The endpoint **GET /stats** return

```json
{
  "error": false,
  "status": 200,
  "mutation": {
    "count_mutations": 40,
    "count_no_mutation": 100,
    "ratio": "0.4"
  }
}
```

- The endpoint **POST /mutation**

```json
// send
{
"dna":["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]
}

// return
{
    "error": false,
    "status": 403,
    "mutation": false
}
```

#### Architecture

These components can be turned into microservices

![general architecture](https://i.imgur.com/BjvA9Sz.png)

**Coverage:**

![coverage](https://i.imgur.com/fRSNQZU.png)
