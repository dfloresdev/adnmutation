require("dotenv").config();

module.exports = {
  marvel: {
    host: process.env.HOST,
    port: process.env.PORT || 3001,
  },
  mongodb: {
    user: process.env.MONGO_USER,
    pass: process.env.MONGO_PASS,
  },
};
