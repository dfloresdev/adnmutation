const db = require("mongoose");
const config = require("../../config");
const ModelDna = require("../api/components/dna/model");

db.Promise = global.Promise;

db.connect(
  `mongodb+srv://${config.mongodb.user}:${config.mongodb.pass}@arca.3rejs.mongodb.net/records`,
  { useUnifiedTopology: true, useNewUrlParser: true },
);

console.info("[DB Connection] successfully");

async function insert(table, data) {
  if (table === "dnaRecords") {
    const dna = new ModelDna(data);
    dna.save();
  }
}

async function getStats() {
  const stats = ModelDna.aggregate([
    {
      $group: {
        _id: "$mutation",
        total: { $sum: 1 },
      },
    },
  ]);

  return stats;
}

module.exports = {
  insert,
  getStats,
};
