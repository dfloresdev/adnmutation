const app = require("./app");
const config = require("../../config");

app.listen(config.api.port, () => {
  console.log(`Ready and app listening at http://localhost:${config.api.port}`);
});
