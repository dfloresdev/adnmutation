const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const dna = require("./components/dna/network");
const errors = require("../network/errors");
const swaggerUI = require("swagger-ui-express");
const swaggerDoc = require("./swagger.json");

const app = express();
const port = 3000;

app.use(cors());
app.use(bodyParser.json());

// ROUTER
app.use("/api/dna", dna);

app.use("/", swaggerUI.serve, swaggerUI.setup(swaggerDoc));

app.use(errors);

module.exports = app;
