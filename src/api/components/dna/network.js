const express = require("express");
const router = express.Router();

const Controller = require("./index");
const response = require("../../../network/response");
const methods = require("./methods");

router.get("/stats", stats);
router.post("/mutation", hasMutation);

function stats(req, res, next) {
  Controller.getStats()
    .then((obj) => {
      let isMutation = 0;
      let mutation = 0;

      if (obj[0]._id) {
        isMutation = obj[0].total;
        mutation = obj[1].total;
      } else {
        isMutation = obj[1].total;
        mutation = obj[0].total;
      }

      const stats = {
        count_mutations: isMutation,
        count_no_mutation: mutation,
        ratio: (isMutation / mutation).toFixed(1),
      };

      response.success(req, res, stats, 200);
    })
    .catch(next);
}

function hasMutation(req, res, next) {
  let dna = req.body.dna;
  let column = [];
  let count = 0;
  let mutation = "";

  do {
    switch (count) {
      case 0:
        column = dna;
        break;
      case 1:
        column = methods.getColumns(dna);
        break;
      case 2:
        column = methods.getColumnsTopLeft(dna);
        break;
      case 3:
        column = methods.getColumnsTopRight(dna);
        break;
    }
    mutation = methods.chainRows(column, count);
    count++;
  } while (!mutation && count <= 3);

  const dnaInsert = Controller.insert(dna, mutation)
    .then((result) => {
      if (mutation) {
        response.success(req, res, true, 200);
      } else {
        response.success(req, res, false, 403);
      }
    })
    .catch(next);
}

module.exports = router;
