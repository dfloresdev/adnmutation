function checkChain(chainDna, count) {
  let patt;
  if (count < 2) {
    patt = /^(?![atcgATCG]*([atcgATCG])\1{3})((^[atcgATCG]{6,})$)/;
  } else {
    patt = /^(?![atcgATCG]*([atcgATCG])\1{3})((^[atcgATCG]{4,})$)/;
  }
  let result = patt.test(chainDna);
  return result;
}

function chainRows(dna, count) {
  let result = dna.find((chain) => checkChain(chain, count) === false);
  return result;
}

function getColumns(dna) {
  let columns = new Array(dna.length).fill("");
  dna.map((chain) => {
    for (let i = 0; i < chain.length; i++) {
      columns[i] += chain[i];
    }
  });
  return columns;
}

function getColumnsTopLeft(dna) {
  const sideways = getSideways(dna.length);
  let columns = new Array(sideways).fill("");
  let position = 3;
  let length = dna.length;
  let flag = false;
  let indexRow = 1;

  dna.map((chain, index) => {
    for (let i = 0; i < length; i++) {
      if (i >= position && !flag) {
        columns[i - position] += chain[i];
      }

      if (flag) {
        columns[position] += chain[i];
        position++;
      }
    }
    if (position > 0 && !flag) {
      position--;
    } else {
      flag = true;
      position = indexRow;
      indexRow++;
    }

    if (index >= dna.length - 4) {
      length--;
    }
  });
  return columns;
}

function getColumnsTopRight(dna) {
  const sideways = getSideways(dna.length);
  let columns = new Array(sideways).fill("");
  let position = 0;
  let acum = 1;
  let length = dna.length;
  let limit = dna.length - 4;
  let start = 0;

  dna.map((chain, index) => {
    for (let i = limit; i >= start; i--) {
      columns[position] += chain[i];
      position++;
    }
    if (index >= length - 4) {
      start++;
      position++;
      if (index >= 3) {
        position = acum;
        acum++;
      } else {
        position = 0;
      }
    } else {
      position = 0;
    }
    if (limit < length - 1) {
      limit++;
    }
  });

  return columns;
}

function getSideways(length) {
  return (length - 6) * 2 + 5;
}

module.exports = {
  checkChain,
  chainRows,
  getColumns,
  getColumnsTopLeft,
  getColumnsTopRight,
  getSideways,
};
