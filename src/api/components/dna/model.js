const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const dnaSchema = new Schema({
  dna: [String],
  mutation: Boolean,
});

const modelDna = mongoose.model("dna", dnaSchema);
module.exports = modelDna;
