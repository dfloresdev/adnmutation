// if you wish injected differents stores, this is the solution

const controller = require("./controller");
const store = require("../../../store/mongodb");

module.exports = controller(store);
