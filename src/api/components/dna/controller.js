var ObjectId = require("mongodb").ObjectID;

const TABLE = "dnaRecords";

module.exports = (injectedStore) => {
  let store = injectedStore;

  async function insert(objDna, isMutation) {
    const data = {
      _id: new ObjectId(),
      dna: objDna,
      mutation: isMutation ? true : false,
    };
    return store.insert(TABLE, data);
  }

  async function getStats() {
    return store.getStats();
  }

  return {
    insert,
    getStats,
  };
};
